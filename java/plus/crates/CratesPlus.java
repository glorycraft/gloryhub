package plus.crates;

import com.google.common.io.ByteStreams;
import fr.glorycraft.hub.GloryHub;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import plus.crates.Commands.CrateCommand;
import plus.crates.Crates.Crate;
import plus.crates.Crates.KeyCrate;
import plus.crates.Handlers.*;
import plus.crates.Listeners.BlockListeners;
import plus.crates.Listeners.InventoryInteract;
import plus.crates.Listeners.PlayerInteract;
import plus.crates.Listeners.PlayerJoin;
import plus.crates.Utils.MCDebug;
import plus.crates.Utils.Version_1_8;
import plus.crates.Utils.Version_Util;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CratesPlus implements Listener {
    private static OpenHandler openHandler;
    public GloryHub plugin;
    private String pluginPrefix = ChatColor.GRAY + "[" + ChatColor.AQUA + "CratesPlus" + ChatColor.GRAY + "] " + ChatColor.RESET;
    private String updateMessage = "";
    private String configBackup = null;
    private boolean updateAvailable = false;
    private File dataFile;
    private File messagesFile;
    private YamlConfiguration dataConfig;
    private YamlConfiguration messagesConfig;
    private ConfigHandler configHandler;
    private CrateHandler crateHandler;
    private MessageHandler messageHandler = new MessageHandler(this);
    private SettingsHandler settingsHandler;
    private HologramHandler hologramHandler;
    private String bukkitVersion = "0.0";
    private Version_Util version_util;
    private ArrayList<UUID> creatingCrate = new ArrayList<>();


    public CratesPlus(GloryHub main) {
        plugin = main;
    }

    public static OpenHandler getOpenHandler() {
        return openHandler;
    }

    public FileConfiguration getConfig() {
        return plugin.getConfig();
    }

    public void saveConfig() {
        plugin.saveConfig();
    }

    public void reloadConfig() {
        plugin.reloadConfig();
    }

    public Logger getLogger() {
        return plugin.getLogger();
    }

    public PluginDescriptionFile getDescription() {
        return plugin.getDescription();
    }

    public void onEnable() {
        Server server = plugin.getServer();
        Pattern pattern = Pattern.compile("(^[^\\-]*)");
        Matcher matcher = pattern.matcher(server.getBukkitVersion());
        if (!matcher.find()) {
            plugin.getLogger().severe("Could not find Bukkit version... Disabling plugin");

            return;
        }
        bukkitVersion = matcher.group(1);

        if (plugin.getConfig().isSet("Bukkit Version"))
            bukkitVersion = plugin.getConfig().getString("Bukkit Version");

        if (versionCompare(bukkitVersion, "1.12") > 0) {
            // This means the plugin is using something newer than the latest tested build... we'll show a warning but carry on as usual
            plugin.getLogger().warning("CratesPlus has not yet been officially tested with Bukkit " + bukkitVersion + " but should still work");
            plugin.getLogger().warning("Please let me know if there are any errors or issues");
        }

        if (versionCompare(bukkitVersion, "1.8") > -1) {
            // Use 1.8 Util
            version_util = new Version_1_8(this);
        } else if (versionCompare(bukkitVersion, "1.7") > -1) {
            // Use Default Util
            plugin.getLogger().warning("CratesPlus does NOT fully support Bukkit 1.7, if you have issues please report them but they may not be fixed");
            version_util = new Version_Util(this);
        } else {
            plugin.getLogger().severe("CratesPlus does NOT support Bukkit " + bukkitVersion + ", if you believe this is an error please let me know");
            if (!plugin.getConfig().isSet("Ignore Version") || !plugin.getConfig().getBoolean("Ignore Version")) { // People should only ignore this in the case of an error, doing an ignore on a unsupported version could break something

                return;
            }
            version_util = new Version_Util(this); // Use the 1.7/1.8 util? Probably has a lower chance of breaking
        }

        final ConsoleCommandSender console = server.getConsoleSender();

        if (plugin.getConfig().getInt("Config Version") == 5) {
            String oldConfig = uploadConfig();
            convertConfigV6(console, oldConfig); // Let me add another one xD ~Xorinzor
        }
        if (plugin.getConfig().getInt("Config Version") == 6) {
            String oldConfig = uploadConfig();
            convertConfigV7(console, oldConfig); // I mean... what was I expecting for v5?
        }
        cleanUpDeadConfig();
        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();

        hologramHandler = new HologramHandler();

        // Check data.yml exists, if not create it!
        dataFile = new File(plugin.getDataFolder(), "data.yml");
        dataConfig = YamlConfiguration.loadConfiguration(dataFile);
        try {
            dataConfig.save(dataFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        updateDataFile();

        // Load new messages.yml
        messagesFile = new File(plugin.getDataFolder(), "messages.yml");
        if (!messagesFile.exists()) {
            try {
                messagesFile.createNewFile();
                InputStream inputStream = plugin.getResource("messages.yml");
                OutputStream outputStream = new FileOutputStream(messagesFile);
                ByteStreams.copy(inputStream, outputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        messagesConfig = YamlConfiguration.loadConfiguration(messagesFile);

        if (!messagesConfig.isSet("Prefix"))
            messagesConfig.set("Prefix", "&7[&bCratesPlus&7]");

        if (!messagesConfig.isSet("Command No Permission"))
            messagesConfig.set("Command No Permission", "&cYou do not have the correct permission to run this command");

        if (!messagesConfig.isSet("Crate No Permission"))
            messagesConfig.set("Crate No Permission", "&cYou do not have the correct permission to use this crate");

        if (!messagesConfig.isSet("Crate Open Without Key"))
            messagesConfig.set("Crate Open Without Key", "&cYou must be holding a %crate% &ckey to open this crate");

        if (!messagesConfig.isSet("Key Given"))
            messagesConfig.set("Key Given", "&aYou have been given a %crate% &acrate key");

        if (!messagesConfig.isSet("Broadcast"))
            messagesConfig.set("Broadcast", "&d%displayname% &dopened a %crate% &dcrate");

        if (!messagesConfig.isSet("Cant Place"))
            messagesConfig.set("Cant Place", "&cYou can not place crate keys");

        if (!messagesConfig.isSet("Cant Drop"))
            messagesConfig.set("Cant Drop", "&cYou can not drop crate keys");

        if (!messagesConfig.isSet("Chance Message"))
            messagesConfig.set("Chance Message", "&d%percentage%% Chance");

        if (!messagesConfig.isSet("Chance Message Gap"))
            messagesConfig.set("Chance Message Gap", true);

        if (!messagesConfig.isSet("Inventory Full Claim"))
            messagesConfig.set("Inventory Full Claim", "&aYou're inventorys is full, you can claim your keys later using /claim");

        if (!messagesConfig.isSet("Claim Join"))
            messagesConfig.set("Claim Join", "&aYou currently have keys waiting to be claimed, use /crate to claim");

        if (!messagesConfig.isSet("Possible Wins Title"))
            messagesConfig.set("Possible Wins Title", "Possible Wins:");

        try {
            messagesConfig.save(messagesFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        processNewMessagesFile();
        configHandler = new ConfigHandler(plugin.getConfig(), this);


        // Load the crate handler
        crateHandler = new CrateHandler(this);

        // Do Prefix
        pluginPrefix = ChatColor.translateAlternateColorCodes('&', messagesConfig.getString("Prefix")) + " " + ChatColor.RESET;

        // Register /crate command
        Bukkit.getPluginCommand("crate").setExecutor(new CrateCommand(this));

        // Register Events
        Bukkit.getPluginManager().registerEvents(new BlockListeners(this), plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerJoin(this), plugin);
        Bukkit.getPluginManager().registerEvents(new InventoryInteract(this), plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerInteract(this), plugin);
//		Bukkit.getPluginManager().registerEvents(new SettingsListener(this), this);
//		Bukkit.getPluginManager().registerEvents(new HologramListeners(this), this);

        openHandler = new OpenHandler(this);

        settingsHandler = new SettingsHandler(this);

        loadMetaData();

        console.sendMessage(ChatColor.AQUA + plugin.getDescription().getName() + " Version " + plugin.getDescription().getVersion());

        switch (getHologramHandler().getHologramPlugin()) {
            default:
            case NONE:
                console.sendMessage(ChatColor.RED + "Unable to find compatible Hologram plugin, holograms will not work!");
                break;
            case HOLOGRAPHIC_DISPLAYS:
                console.sendMessage(ChatColor.GREEN + "HolographicDisplays was found, hooking in!");
                break;
            case INDIVIDUAL_HOLOGRAMS:
                console.sendMessage(ChatColor.GREEN + "IndividualHolograms was found, hooking in!");
                break;
        }

        if (configBackup != null && Bukkit.getOnlinePlayers().size() > 0) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.hasPermission("cratesplus.admin")) {
                    player.sendMessage(pluginPrefix + ChatColor.GREEN + "Your config has been updated. Your old config was backed up to " + configBackup);
                    configBackup = null;
                }
            }
        }


    }

    private void processNewMessagesFile() {
        if (plugin.getConfig().isSet("Messages")) {
            for (String path : plugin.getConfig().getConfigurationSection("Messages").getKeys(false)) {
                messagesConfig.set(path, plugin.getConfig().getString("Messages." + path));
            }
            try {
                messagesConfig.save(messagesFile);
                plugin.getConfig().set("Messages", null);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onDisable() {
    }

    private void cleanUpDeadConfig() {
        if (plugin.getConfig().isSet("More Info Hologram"))
            plugin.getConfig().set("More Info Hologram", null);
        if (plugin.getConfig().isSet("Enable GUI Beta Animation"))
            plugin.getConfig().set("Enable GUI Beta Animation", null);
    }

    public String uploadConfig() {
        return uploadFile("config.yml");
    }

    public String uploadData() {
        return uploadFile("data.yml");
    }

    public String uploadMessages() {
        return uploadFile("messages.yml");
    }

    public String uploadFile(String fileName) {
        File file = new File(plugin.getDataFolder(), fileName);
        if (!file.exists())
            return null;
        LineIterator it;
        String lines = "";
        try {
            it = FileUtils.lineIterator(file, "UTF-8");
            try {
                while (it.hasNext()) {
                    String line = it.nextLine();
                    lines = lines + line + "\n";
                }
            } finally {
                it.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return MCDebug.paste(fileName, lines);
    }

    public void reloadPlugin() {
        plugin.reloadConfig();

        // Do Prefix
        pluginPrefix = ChatColor.translateAlternateColorCodes('&', messagesConfig.getString("Prefix")) + " " + ChatColor.RESET;

        // Reload Configuration
        configHandler = new ConfigHandler(plugin.getConfig(), this);

        // Settings Handler
        settingsHandler = new SettingsHandler(this);

    }

    private void loadMetaData() {
        if (!dataConfig.isSet("Crate Locations"))
            return;
        for (String name : dataConfig.getConfigurationSection("Crate Locations").getKeys(false)) {
            final Crate crate = configHandler.getCrate(name.toLowerCase());
            if (crate == null)
                continue;
            if (!(crate instanceof KeyCrate))
                continue;
            KeyCrate keyCrate = (KeyCrate) crate;
            String path = "Crate Locations." + name;
            List<String> locations = dataConfig.getStringList(path);

            for (String location : locations) {
                List<String> strings = Arrays.asList(location.split("\\|"));
                if (strings.size() < 4)
                    continue; // Somethings broke?
                if (strings.size() > 4) {
                    // Somethings broke? But we'll try and fix it!
                    for (int i = 0; i < strings.size(); i++) {
                        if (strings.get(i).isEmpty() || strings.get(i).equals("")) {
                            strings.remove(i);
                        }
                    }
                }
                Location locationObj;
                try {
                    locationObj = new Location(Bukkit.getWorld(strings.get(0)), Double.parseDouble(strings.get(1)), Double.parseDouble(strings.get(2)), Double.parseDouble(strings.get(3)));
                    Block block = locationObj.getBlock();
                    if (block == null || block.getType().equals(Material.AIR)) {
                        plugin.getLogger().warning("No block found at " + location + " removing from data.yml");
                        keyCrate.removeFromConfig(locationObj);
                        continue;
                    }
                    Location location1 = locationObj.getBlock().getLocation().add(0.5, 0.5, 0.5);
                    keyCrate.loadHolograms(location1);
                    final CratesPlus cratesPlus = this;
                    block.setMetadata("CrateType", new MetadataValue() {
                        @Override
                        public Object value() {
                            return crate.getName(false);
                        }

                        @Override
                        public int asInt() {
                            return 0;
                        }

                        @Override
                        public float asFloat() {
                            return 0;
                        }

                        @Override
                        public double asDouble() {
                            return 0;
                        }

                        @Override
                        public long asLong() {
                            return 0;
                        }

                        @Override
                        public short asShort() {
                            return 0;
                        }

                        @Override
                        public byte asByte() {
                            return 0;
                        }

                        @Override
                        public boolean asBoolean() {
                            return false;
                        }

                        @Override
                        public String asString() {
                            return value().toString();
                        }

                        @Override
                        public Plugin getOwningPlugin() {
                            return plugin;
                        }

                        @Override
                        public void invalidate() {

                        }
                    });
                } catch (Exception ignored) {
                }
            }


        }
    }

    private void updateDataFile() {
        if (!dataConfig.isSet("Data Version") || dataConfig.getInt("Data Version") == 1) {
            dataConfig.set("Data Version", 2);
            if (dataConfig.isSet("Crate Locations"))
                dataConfig.set("Crate Locations", null);
            try {
                dataConfig.save(dataFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public SettingsHandler getSettingsHandler() {
        return settingsHandler;
    }

    public String getPluginPrefix() {
        if (getMessageHandler().isAprilFools()) {
            return ChatColor.LIGHT_PURPLE + ChatColor.stripColor(pluginPrefix);
        }
        return pluginPrefix;
    }

    public ConfigHandler getConfigHandler() {
        return configHandler;
    }

    public HologramHandler getHologramHandler() {
        return hologramHandler;
    }

    public YamlConfiguration getDataConfig() {
        return dataConfig;
    }

    public File getDataFile() {
        return dataFile;
    }

    public String getUpdateMessage() {
        return updateMessage;
    }

    public String getConfigBackup() {
        return configBackup;
    }

    public void setConfigBackup(String configBackup) {
        this.configBackup = configBackup;
    }

    public Version_Util getVersion_util() {
        return version_util;
    }

    public File getMessagesFile() {
        return messagesFile;
    }

    public YamlConfiguration getMessagesConfig() {
        return messagesConfig;
    }

    public boolean isUpdateAvailable() {
        return updateAvailable;
    }

    public MessageHandler getMessageHandler() {
        return messageHandler;
    }

    public CrateHandler getCrateHandler() {
        return crateHandler;
    }

    private void convertConfigV6(ConsoleCommandSender console, String oldConfig) {
        console.sendMessage(pluginPrefix + ChatColor.GREEN + "Converting config to version 6...");

        if (plugin.getConfig().isSet("Hologram Text")) {
            List<String> oldHologramList = plugin.getConfig().getStringList("Hologram Text");
            plugin.getConfig().set("Default Hologram Text", oldHologramList);
            plugin.getConfig().set("Hologram Text", null);
        }

        // Set config version
        plugin.getConfig().set("Config Version", 6);

        // Save config
        plugin.saveConfig();

        console.sendMessage(pluginPrefix + ChatColor.GREEN + "Conversion of config has completed.");
        if (oldConfig != null && !oldConfig.equalsIgnoreCase("")) {
            configBackup = oldConfig;
            console.sendMessage(pluginPrefix + ChatColor.GREEN + "Your old config was backed up to " + oldConfig);
        }
    }

    private void convertConfigV7(ConsoleCommandSender console, String oldConfig) {
        console.sendMessage(pluginPrefix + ChatColor.GREEN + "Converting config to version 7...");

        if (plugin.getConfig().isSet("Crates")) {
            for (String crate : plugin.getConfig().getConfigurationSection("Crates").getKeys(false)) {
                if (!plugin.getConfig().isSet("Crates." + crate + ".Type")) {
                    plugin.getConfig().set("Crates." + crate + ".Type", "KeyCrate");
                }
            }
        }

        // Set config version
        plugin.getConfig().set("Config Version", 7);

        // Save config
        plugin.saveConfig();

        console.sendMessage(pluginPrefix + ChatColor.GREEN + "Conversion of config has completed.");
        if (oldConfig != null && !oldConfig.equalsIgnoreCase("")) {
            configBackup = oldConfig;
            console.sendMessage(pluginPrefix + ChatColor.GREEN + "Your old config was backed up to " + oldConfig);
        }
    }

    // System.out.println(versionCompare("1.6", "1.8")); // -1 as 1.8 is newer
    // System.out.println(versionCompare("1.7", "1.8")); // -1 as 1.8 is newer
    // System.out.println(versionCompare("1.8", "1.8")); // 0 as same
    // System.out.println(versionCompare("1.9", "1.8")); // 1 as 1.9 is newer
    public int versionCompare(String str1, String str2) {
        String[] vals1 = str1.split("\\.");
        String[] vals2 = str2.split("\\.");
        int i = 0;
        while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
            i++;
        }
        if (i < vals1.length && i < vals2.length) {
            int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
            return Integer.signum(diff);
        }
        return Integer.signum(vals1.length - vals2.length);
    }

    public String getBukkitVersion() {
        return bukkitVersion;
    }

    public ArrayList<UUID> getCreatingCrate() {
        return creatingCrate;
    }

    public boolean isCreating(UUID uuid) {
        return creatingCrate.contains(uuid);
    }

    public void addCreating(UUID uuid) {
        creatingCrate.add(uuid);
    }

    public void removeCreating(UUID uuid) {
        creatingCrate.remove(uuid);
    }

}
