package fr.glorycraft.randomtp.command;

import fr.glorycraft.hub.GloryHub;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Random;

public class RandomTPCommand implements CommandExecutor {
    GloryHub plugin;

    public RandomTPCommand(GloryHub main) {
        this.plugin = main;
    }

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            Player p = (Player) commandSender;
            Random random = new Random();
            Location teleportLocation;
            int x = random.nextInt(10000) + 1;
            int y = 150;
            int z = random.nextInt(10000) + 1;
            boolean isOnLand = false;

            while (!isOnLand) {
                teleportLocation = new Location(p.getWorld(), (double) x, (double) y, (double) z);
                if (teleportLocation.getBlock().getType() != Material.AIR) {
                    isOnLand = true;
                } else {
                    --y;
                }
            }

            (new Location(p.getWorld(), (double) x, (double) y, (double) z)).getChunk().load();
            p.teleport(new Location(p.getWorld(), (double) x, (double) y, (double) z));
            p.sendMessage("§c§[§e§lRandom§7§l-§b§lTP§c§l] §b§lVous avez été TP en §8§x§c§l" + x + "§8§ly:§c§l" + y + "§8§lz:§c§l" + z);
            return true;
        } else {
            return false;
        }
    }
}
