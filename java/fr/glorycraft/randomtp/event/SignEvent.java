package fr.glorycraft.randomtp.event;


import fr.glorycraft.hub.GloryHub;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Random;

public class SignEvent implements Listener {
    GloryHub plugin;

    public SignEvent(GloryHub main) {
        this.plugin = main;
    }

    @EventHandler
    public void onPlaceSign(SignChangeEvent e) {
        if (e.getLine(0).contains("RandomTP") && e.getPlayer().hasPermission("random.sign")) {
            e.setLine(0, "§7§[§b§lRandom§c§lTP");
            e.setLine(1, "§a§lClique ici pour");
            e.setLine(2, "§e§lêtre téléporter");
            e.setLine(3, "§8§laléatoirement !");
        }

    }

    @EventHandler
    public void onClickSign(PlayerInteractEvent e) {
        Action a = e.getAction();
        Block b = e.getClickedBlock();
        Player p = e.getPlayer();
        if (b != null && p != null && b.getState() instanceof Sign) {
            Sign s = (Sign) b.getState();
            if (s.getLine(0).contains("§b§lRandom§c§lTP")) {
                Random random = new Random();
                Location teleportLocation;
                int x = random.nextInt(10000) + 1;
                int y = 150;
                int z = random.nextInt(10000) + 1;
                boolean isOnLand = false;

                while (!isOnLand) {
                    teleportLocation = new Location(p.getWorld(), (double) x, (double) y, (double) z);
                    if (teleportLocation.getBlock().getType() != Material.AIR) {
                        isOnLand = true;
                    } else {
                        --y;
                    }
                }

                (new Location(p.getWorld(), (double) x, (double) y, (double) z)).getChunk().load();
                p.teleport(new Location(p.getWorld(), (double) x, (double) y, (double) z));
                p.sendMessage("§c§[§e§lRandom§7§l-§b§lTP§c§l] §b§lVous avez été TP en §8§x§c§l" + x + "§8§ly:§c§l" + y + "§8§lz:§c§l" + z);
            }
        }

    }
}
