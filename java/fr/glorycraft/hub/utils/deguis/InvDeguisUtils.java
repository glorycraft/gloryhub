package fr.glorycraft.hub.utils.deguis;


import fr.glorycraft.hub.utils.InventoryEvent;
import net.minecraft.server.v1_8_R3.WorldServer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class InvDeguisUtils {
    public InvDeguisUtils(InventoryEvent.dType type, Player player, ItemStack item) {
        switch(type) {
            case DEGUIS:
                this.deguis(player, item);
                break;
            case RIDE:
                this.montures(player, item);
        }

    }

    public void deguis(Player p, ItemStack i) {
        ItemStack ime = new ItemStack(Material.SKULL_ITEM, 1, (short)1);
        ItemMeta imm = ime.getItemMeta();
        imm.setDisplayName("Creeper");
        ime.setItemMeta(imm);
        if (i.isSimilar(ime)) {
            p.closeInventory();
        }

    }

    public void montures(Player p, ItemStack i) {
        EntityType[] var3 = EntityType.values();
        int var4 = var3.length;

        for(int var5 = 0; var5 < var4; ++var5) {
            EntityType e1 = var3[var5];
            switch(e1) {
                case HORSE:
                    ItemStack i2 = new ItemStack(Material.SADDLE, 1);
                    ItemMeta i2m = i2.getItemMeta();
                    i2m.setDisplayName("Cheval");
                    i2.setItemMeta(i2m);
                    if (i.isSimilar(i2)) {
                        p.closeInventory();
                        Horse h = (Horse)p.getLocation().getWorld().spawnCreature(p.getLocation(), EntityType.HORSE);
                        h.setPassenger(p);
                        h.getInventory().setSaddle(new ItemStack(Material.SADDLE, 1));
                    }
                    break;
                case SHEEP:
                    ItemStack i3 = new ItemStack(Material.WOOL);
                    ItemMeta i3m = i3.getItemMeta();
                    i3m.setDisplayName(ChatColor.WHITE + "Moutons");
                    i3.setItemMeta(i3m);
                    if (i.isSimilar(i3)) {
                        p.closeInventory();
                        WorldServer nm = ((CraftWorld)p.getLocation().getWorld()).getHandle();
                        //Sheep m=
                      //  m.setPosition(p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ());
                      //  nm.addEntity(m);
                      //  ((CraftPlayer)p).getHandle().mount(m);
                    }
                    break;
                case WOLF:
                    ItemStack i4 = new ItemStack(Material.BONE);
                    ItemMeta i4m = i4.getItemMeta();
                    i4m.setDisplayName(ChatColor.GREEN + "Loup");
                    i4.setItemMeta(i4m);
                    if (i.isSimilar(i4)) {
                        p.closeInventory();
                        WorldServer nm = ((CraftWorld)p.getLocation().getWorld()).getHandle();
                      /*  Wolf1 m = new Wolf1(nm);
                        m.setPosition(p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ());
                        nm.addEntity(m);
                        ((CraftPlayer)p).getHandle().mount(m);*/
                    }
                    break;
                case PIG:
                    ItemStack i5 = new ItemStack(Material.PORK, 1);
                    ItemMeta i5m = i5.getItemMeta();
                    i5m.setDisplayName(ChatColor.RED + "Cochon");
                    i5.setItemMeta(i5m);
                    if (i.isSimilar(i5)) {
                        p.closeInventory();
                        WorldServer nm = ((CraftWorld)p.getLocation().getWorld()).getHandle();
                       /* Pig1 m = new Pig1(nm);
                        m.setPosition(p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ());
                        nm.addEntity(m);
                        ((CraftPlayer)p).getHandle().mount(m);*/
                    }
                    break;
                default:
                    if (e1.isAlive()) {
                        ItemStack i6 = new ItemStack(Material.MONSTER_EGG, 1, e1.getTypeId());
                        ItemMeta i6m = i6.getItemMeta();
                        i6m.setDisplayName(ChatColor.RED + e1.getName() + " SOON");
                        i6m.setLore(Arrays.asList("SOON"));
                        i6.setItemMeta(i6m);
                        if (i.isSimilar(i6)) {
                            p.closeInventory();
                        }
                    }
            }
        }

    }
}

