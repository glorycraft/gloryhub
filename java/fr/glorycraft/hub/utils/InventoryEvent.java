package fr.glorycraft.hub.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class InventoryEvent {
    ItemStack item;
    Player player;

    public InventoryEvent(ItemStack item, Player p) {
        this.item = item;
        this.player = p;
    }

    public void deguisInv(InventoryEvent.dType type) {
        new InvDeguisUtils(type, this.player, this.item);
    }

    public void openPets() {
        ItemStack i = new ItemStack(Material.DIAMOND_BARDING, 1);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(ChatColor.AQUA + "Vos montures.");
        i.setItemMeta(im);
        Inventory i1;
        if (this.item.isSimilar(i)) {
            i1 = Bukkit.createInventory((InventoryHolder)null, 54, ChatColor.AQUA + "Vos Montures.");
            EntityType[] var4 = EntityType.values();
            int var5 = var4.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                EntityType e1 = var4[var6];
                switch(e1) {
                    case HORSE:
                        i = new ItemStack(Material.SADDLE, 1);
                        im = i.getItemMeta();
                        im.setDisplayName("Cheval");
                        i.setItemMeta(im);
                        if (this.item.isSimilar(i)) {
                            Horse h = (Horse)this.player.getLocation().getWorld().spawnCreature(this.player.getLocation(), EntityType.HORSE);
                            h.setPassenger(this.player);
                            h.getInventory().setSaddle(new ItemStack(Material.SADDLE, 1));
                            i1.addItem(new ItemStack[]{i});
                        }
                        break;
                    case SHEEP:
                        i = new ItemStack(Material.WOOL);
                        im = i.getItemMeta();
                        im.setDisplayName(ChatColor.WHITE + "Moutons");
                        i.setItemMeta(im);
                        i1.addItem(new ItemStack[]{i});
                        break;
                    case WOLF:
                        i = new ItemStack(Material.BONE);
                        im = i.getItemMeta();
                        im.setDisplayName(ChatColor.GREEN + "Loup");
                        i.setItemMeta(im);
                        i1.addItem(new ItemStack[]{i});
                        break;
                    case PIG:
                        i = new ItemStack(Material.PORK, 1);
                        im = i.getItemMeta();
                        im.setDisplayName(ChatColor.RED + "Cochon");
                        i.setItemMeta(im);
                        i1.addItem(new ItemStack[]{i});
                        break;
                    default:
                        if (e1.isAlive()) {
                            ItemStack i6 = new ItemStack(Material.MONSTER_EGG, 1, e1.getTypeId());
                            ItemMeta i6m = i6.getItemMeta();
                            i6m.setDisplayName(ChatColor.RED + e1.getName() + " SOON");
                            i6m.setLore(Arrays.asList("SOON"));
                            i6.setItemMeta(i6m);
                        }
                }
            }

            this.player.openInventory(i1);
        }

        i = new ItemStack(Material.MONSTER_EGG, 1);
        im = i.getItemMeta();
        im.setDisplayName("Métamorphoses (Bêta)");
        i.setItemMeta(im);
        if (this.item.isSimilar(i)) {
            i1 = Bukkit.createInventory(null, 9, ChatColor.AQUA + "Vos Métamorphoses.");
            ItemStack ime = new ItemStack(Material.SKULL_ITEM, 1, (short)1);
            ItemMeta imm = ime.getItemMeta();
            if (this.player.isOp()) {
                imm.setDisplayName("Creeper");
            } else {
                imm.setDisplayName("Creeper " + ChatColor.GRAY + "(" + ChatColor.RED + "Réserver au Staff" + ChatColor.GRAY + ")");
            }

            ime.setItemMeta(imm);
            i1.addItem(new ItemStack[]{ime});
            ime = new ItemStack(Material.BLAZE_ROD, 1);
            imm = ime.getItemMeta();
            imm.setDisplayName("Blaze (POUF TU CRAME)");
            ime.setItemMeta(imm);
            i1.addItem(new ItemStack[]{ime});
            this.player.openInventory(i1);
        }

        i = new ItemStack(Material.DRAGON_EGG, 1);
        im = i.getItemMeta();
        im.setDisplayName("Famillier");
        i.setItemMeta(im);
    }

    public static enum dType {
        RIDE,
        DEGUIS,
        HUB,
        FAM;

        private dType() {
        }
    }
}

