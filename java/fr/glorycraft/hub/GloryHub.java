package fr.glorycraft.hub;

import fr.glorycraft.hub.events.BlockListener;
import fr.glorycraft.hub.events.EntityListener;
import fr.glorycraft.hub.events.OtherEvent;
import fr.glorycraft.hub.items.ItemCompass;
import fr.glorycraft.hub.items.ItemShop;
import fr.glorycraft.randomtp.command.RandomTPCommand;
import fr.glorycraft.randomtp.event.SignEvent;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;
import plus.crates.CratesPlus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

public class GloryHub extends JavaPlugin {
    public static Hashtable<Integer, ItemStack> ItemsCompass = null;
    public static Hashtable<Integer, ItemStack> ItemsShop = null;
    //public TinyProtocol protocol;
    public HashMap<Player, Effect> eff = new HashMap();
    // public GloryAPI api;
    public GloryHub instance;
    public ArrayList<String> pvpbox = new ArrayList();
    public ArrayList<String> onArene = new ArrayList();
    public HashMap<Player, Inventory> inv = new HashMap();
    int sec = 0;
    int min = 0;
    private BukkitTask task;
    private CratesPlus crate = new CratesPlus(this);

    public GloryHub() {
    }

    public void onLoad() {
    }

    public Location str2loc(String str) {
        String[] str2loc = str.split(":");
        Location loc = new Location(this.getServer().getWorld(str2loc[0]), 0.0D, 0.0D, 0.0D);
        loc.setX(Double.parseDouble(str2loc[1]));
        loc.setY(Double.parseDouble(str2loc[2]));
        loc.setZ(Double.parseDouble(str2loc[3]));
        return loc;
    }

    public void onEnable() {
        crate = new CratesPlus(this);
        crate.onEnable();
//hdv ptn jai oublié
        /* Random Teleportation Register*/
        getCommand("randomtp").setExecutor(new RandomTPCommand(this));
        getServer().getPluginManager().registerEvents(new SignEvent(this), this);

        /* Hub Register*/
        getServer().getPluginManager().registerEvents(new BlockListener(this), this);
        getServer().getPluginManager().registerEvents(new EntityListener(this), this);
        getServer().getPluginManager().registerEvents(new OtherEvent(this), this);

        ItemCompass I = new ItemCompass(this, null);
        ItemsCompass = I.BuildItems();

        ItemShop I1 = new ItemShop(this, null);
        ItemsShop = I1.BuildItems();
     /*   this.api = GloryAPI.instance.GloryInstance(ChatColor.AQUA + "%world%", "hub");
        this.api.console.info("Allumage de GloryHub terminé");
        this.api.console.otherSrv.setName("GloyHub");
        this.instance = this;
        this.getServer().getPluginManager().registerEvents(new PlayerListener(this), this);

        this.getCommand("set").setExecutor(new Commanset(this));
        this.getCommand("setitem").setExecutor(new CommandSetItem(this));
        this.getCommand("settitle").setExecutor(new CommandSetTitle(this));
        this.getCommand("giveglory").setExecutor(new CommandGiveGloryEuro(this));
        this.getCommand("reloadg").setExecutor(new CommandReload(this));

        this.api.console.otherSrv.plugin_start();
        this.task = Bukkit.getScheduler().runTaskTimer(this, this, 0L, 20L);*/

    }

    public void run() {
        ++this.sec;
        if (this.sec == 60) {
            this.sec = 0;
            ++this.min;
            if (this.min != 5) {
                Bukkit.broadcastMessage("§c§lDestruction des mobs et entité dans " + (5 - this.min) + " minute(s) !\n §7§l(§a§lpour évité les lags§7§l)");
            }
        }

        boolean unload = true;
        if (this.min == 5) {
            this.min = 0;
            Bukkit.broadcastMessage("§c§lDestruction des mobs et entités  !");


            for (World w : Bukkit.getWorlds()) {


                for (Entity e : w.getEntities()) {

                    if (e.getType() != EntityType.ARMOR_STAND && e.getType() != EntityType.PLAYER && !e.getType().name().contains("Minecart") && e.getType() != EntityType.WOLF && e.getType() != EntityType.HORSE && e.getType() != EntityType.VILLAGER) {
                        ((CraftWorld) w).getHandle().removeEntity(((CraftEntity) e).getHandle());
                    }
                }
            }


        }


        for (Player p : Bukkit.getOnlinePlayers()) {
            if (this.eff != null) {
                Effect ef = this.eff.get(p);
                Location l = p.getLocation();

                for (int i = 100; i >= 0; --i) {
                    p.getWorld().playEffect(l, ef, 5);
                }
            }
        }


    }

    public void onDisable() {
        this.crate.onDisable();
        //  this.task.cancel();
    }
}
