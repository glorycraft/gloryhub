package fr.glorycraft.hub.inventorys;

import fr.glorycraft.hub.GloryHub;
import fr.glorycraft.hub.items.ItemShop;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.Hashtable;
import java.util.Map;

public class InventoryShop {
    public static Player _player = null;
    static GloryHub plugin;
    private Hashtable<Integer, ItemStack> _items = null;
    private Inventory _Inventory = null;

    public InventoryShop(Player p, Hashtable<Integer, ItemStack> items, GloryHub gloryHub) {
        plugin = gloryHub;
        FileConfiguration f = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "shop.yml"));
        _player = p;
        this._items = items;
        String title = f.getString("title");
        title = title.replace("%ge", "");
        title = title.replace("%gc", "");
        _Inventory = Bukkit.createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', title));
        Build();
    }

    private void Build() {


        for (Map.Entry<Integer, ItemStack> i : _items.entrySet()) {
            this._Inventory.setItem(i.getKey(), i.getValue());
        }

        ItemShop.player = _player;
    }

    public void Open() {
        _player.openInventory(this._Inventory);
    }
}
