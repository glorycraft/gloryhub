package fr.glorycraft.hub.inventorys;

import fr.glorycraft.hub.GloryHub;
import fr.glorycraft.hub.items.ItemCompass;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.Hashtable;
import java.util.Map;

public class InventoryCompass {
    private Player _player = null;
    private Hashtable<Integer, ItemStack> _items = null;
    private Inventory _Inventory = null;
    private GloryHub plugin;

    public InventoryCompass(Player p, Hashtable<Integer, ItemStack> items, GloryHub gloryHub) {
        plugin = gloryHub;
        FileConfiguration f = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "games.yml"));
        _player = p;
        this._items = items;
        String title = ChatColor.translateAlternateColorCodes('&', f.getString("title"));
        switch (title) {
            case "=>":
                title = title.replace("=>", "➡");
                break;
            case "<=":
                title = title.replace("<=", "⬅");
                break;
            case "^|":
                title = title.replace("^|", "⬆");
                break;
            case "|^":
                title = title.replace("|^", "⬇");
                break;
            case "<3":
                title = title.replace("<3", "❤");
                break;
            case "[x]":
                title = title.replace("[x]", "█");
                break;
            case "[++]":
                title = title.replace("[++]", "✦");
                break;
            case "[+]":
                title = title.replace("[+]", "◆");
                break;
            case "[p]":
                title = title.replace("[p]", "•");
                break;
            case "[/]":
                title = title.replace("[/]", "▌");
                break;
            case "[*]":
                title = title.replace("[*]", "★");
                break;
            case "[**]":
                title = title.replace("[**]", "✹");
                break;
            case "[v]":
                title = title.replace("[v]", "✔");
                break;
            case "[c]":
                title = title.replace("[c]", "✠");
                break;
            case "%ge":
                title = title.replace("%ge", "§e" + -1);
                break;
            case "%gc":
                title = title.replace("%gc", "§e" + -1);
                break;
        }

        this._Inventory = Bukkit.createInventory(null, 54, title);
        this.Build();
    }

    private void Build() {


        for (Map.Entry<Integer, ItemStack> i : _items.entrySet()) {
            this._Inventory.setItem(i.getKey(), i.getValue());
        }

        ItemCompass.player = _player;
    }

    public void Open() {
        _player.openInventory(this._Inventory);
    }
}

