package fr.glorycraft.hub.inventorys;

import fr.glorycraft.hub.GloryHub;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class InventoryCustom {
    /*TODO FUTUR CLASS FOR CUSTOM INVENTORY FOR DELETE INVCOMPASS & INVSHOP !*/
    private Inventory inv;

    public InventoryCustom(FileConfiguration f, Player p, GloryHub plugin, String secS) {
        String title = ChatColor.translateAlternateColorCodes('&', f.getString("title"));
        if (title.contains("=>")) {
            title = title.replace("=>", "➡");
        }

        if (title.contains("<=")) {
            title = title.replace("<=", "⬅");
        }

        if (title.contains("^|")) {
            title = title.replace("^|", "⬆");
        }

        if (title.contains("|^")) {
            title = title.replace("|^", "⬇");
        }

        if (title.contains("<3")) {
            title = title.replace("<3", "❤");
        }

        if (title.contains("[x]")) {
            title = title.replace("[x]", "█");
        }

        if (title.contains("[++]")) {
            title = title.replace("[++]", "✦");
        }

        if (title.contains("[+]")) {
            title = title.replace("[+]", "◆");
        }

        if (title.contains("[p]")) {
            title = title.replace("[p]", "•");
        }

        if (title.contains("[/]")) {
            title = title.replace("[/]", "▌");
        }

        if (title.contains("[*]")) {
            title = title.replace("[*]", "★");
        }

        if (title.contains("[**]")) {
            title = title.replace("[**]", "✹");
        }

        if (title.contains("[v]")) {
            title = title.replace("[v]", "✔");
        }

        if (title.contains("[c]")) {
            title = title.replace("[c]", "✠");
        }

        this.inv = Bukkit.createInventory((InventoryHolder)null, 54, title);
        ConfigurationSection sec = f.getConfigurationSection(secS);
        if (sec != null) {
            Iterator var7 = sec.getKeys(false).iterator();

            while(var7.hasNext()) {
                String s = (String)var7.next();
                ConfigurationSection items = sec.getConfigurationSection(s);

                try {
                    int place = Integer.parseInt(s);
                    String[] info = items.getString("id").contains(":") ? items.getString("id").split(":") : null;
                    int id = info != null ? Integer.parseInt(info[0]) : Integer.parseInt(items.getString("id"));
                    Random r = new Random();
                    byte data = info != null ? (info[1].equals("r") ? (byte)r.nextInt(15) : (byte)Integer.parseInt(info[1])) : 0;
                    Material m = Material.getMaterial(id);
                    int amount = items.getInt("amount");
                    String dura = items.getString("durab");
                    String itemName = items.get("name") != null ? items.getString("name") : "ERROR 404";
                    String playerName = p.getPlayerListName();
                    ArrayList<String> lores = (ArrayList)items.getStringList("lores");
                    this.inv.setItem(place, this.items(m, itemName, playerName, lores, amount, data, dura));
                } catch (Exception var21) {
                    plugin.api.console.warning("Impossible de crée l'item :" + items.getString("name") + " car " + var21.getMessage() + "slots" + s + ")");
                }
            }
        }

    }

    public ItemStack items(Material m, String name, String owner, ArrayList<String> lore, int amount, byte data, String durability) {
        name = ChatColor.translateAlternateColorCodes('&', name);
        if (name.contains("=>")) {
            name = name.replace("=>", "➡");
        }

        if (name.contains("<=")) {
            name = name.replace("<=", "⬅");
        }

        if (name.contains("^|")) {
            name = name.replace("^|", "⬆");
        }

        if (name.contains("|^")) {
            name = name.replace("|^", "⬇");
        }

        if (name.contains("<3")) {
            name = name.replace("<3", "❤");
        }

        if (name.contains("[x]")) {
            name = name.replace("[x]", "█");
        }

        if (name.contains("[++]")) {
            name = name.replace("[++]", "✦");
        }

        if (name.contains("[+]")) {
            name = name.replace("[+]", "◆");
        }

        if (name.contains("[p]")) {
            name = name.replace("[p]", "•");
        }

        if (name.contains("[/]")) {
            name = name.replace("[/]", "▌");
        }

        if (name.contains("[*]")) {
            name = name.replace("[*]", "★");
        }

        if (name.contains("[**]")) {
            name = name.replace("[**]", "✹");
        }

        if (name.contains("[v]")) {
            name = name.replace("[v]", "✔");
        }

        if (name.contains("[c]")) {
            name = name.replace("[c]", "✠");
        }

        if (name.contains("%ge")) {
            name = name.replace("%ge", ChatColor.YELLOW + "");
        }

        if (name.contains("%gc")) {
            name = name.replace("%gc", ChatColor.YELLOW + "");
        }

        ItemStack item = new ItemStack(m, amount, (short)data);
        ItemMeta meta = item.getItemMeta();
        SkullMeta smeta = null;
        if (m == Material.SKULL || m == Material.SKULL_ITEM) {
            smeta = (SkullMeta)Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
            smeta.setOwner(owner);
        }

        if (lore != null) {
            List<String> lores = new ArrayList();
            Iterator var12 = lore.iterator();

            while(var12.hasNext()) {
                String l = (String)var12.next();
                lores.add(ChatColor.translateAlternateColorCodes('&', l));
            }

            if (smeta != null) {
                smeta.setLore(lores);
            } else {
                meta.setLore(lores);
            }
        }

        if (smeta != null) {
            smeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
            item.setItemMeta(smeta);
        } else {
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
            item.setItemMeta(meta);
        }

        if (durability != null) {
            item.setDurability(Short.parseShort(durability));
        }

        return item;
    }

    public Inventory getInv() {
        return this.inv;
    }
}

