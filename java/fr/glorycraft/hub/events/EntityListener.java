package fr.glorycraft.hub.events;

import fr.glorycraft.hub.GloryHub;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class EntityListener implements Listener {
    GloryHub plugin;

    public EntityListener(GloryHub gloryHub) {
        this.plugin = gloryHub;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onRainStart(WeatherChangeEvent event) {
        if (event.getWorld().getName().contains("Hub")) {
            event.setCancelled(true);
        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        if (event.getEntity().getWorld().getName().contains("Hub")) {
            event.setCancelled(true);
        }

    }
}
