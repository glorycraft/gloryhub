package fr.glorycraft.hub.events;

import fr.glorycraft.hub.GloryHub;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockListener implements Listener {
    // private GloryHub plugin;

    public BlockListener(GloryHub gloryHub) {
        // this.plugin = gloryHub;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.getPlayer().getWorld().getName().contains("Hub")) {
            if (!event.getPlayer().hasPermission("hub.build") && !event.getPlayer().isOp()) {
                event.setCancelled(true);
            } else {
                event.setCancelled(false);
            }
        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockplace(BlockPlaceEvent event) {
        if (event.getPlayer().getWorld().getName().contains("Hub")) {
            if (!event.getPlayer().hasPermission("hub.build") && !event.getPlayer().isOp()) {
                event.setCancelled(true);
            } else {
                event.setCancelled(false);
            }
        }

    }
}
