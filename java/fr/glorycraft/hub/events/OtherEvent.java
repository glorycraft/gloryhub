package fr.glorycraft.hub.events;

import fr.glorycraft.hub.GloryHub;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;

public class OtherEvent implements Listener {
    GloryHub plugin;

    public OtherEvent(GloryHub gloryHub) {
        this.plugin = gloryHub;
    }

    @EventHandler(
            priority = EventPriority.HIGH
    )
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getEntity().getWorld().getName().contains("Hub")) {
            event.setCancelled(true);
        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.getEntity().getWorld().getName().contains("Hub")) {
            event.setCancelled(true);
        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void on(VehicleExitEvent e) {
        if (e.getExited().getWorld().getName().contains("Hub")) {
            e.getVehicle().remove();
        }

    }
}
