package fr.glorycraft.hub.events.players;

import fr.glorycraft.hub.GloryHub;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class PlayerJoinAndLeaveListener {

    static ItemStack setting;
    static ItemStack game;
    static ItemStack gadgets;
    static ItemStack shop;
    private GloryHub plugin;

    public PlayerJoinAndLeaveListener(GloryHub main) {
        plugin = main;
    }

    @EventHandler
    public void on(PlayerQuitEvent e) {
        String[] info;
        for (String s : plugin.pvpbox) {

            info = s.split(":");
            if (info[0].contains(e.getPlayer().getName())) {
                plugin.pvpbox.remove(s);
            }
        }


        for (String s : plugin.onArene) {

            info = s.split(":");
            String[] name_player = info[1].split("-");
            Player p;
            if (name_player[0].equalsIgnoreCase(e.getPlayer().getName())) {
                plugin.onArene.remove(s);
                e.getPlayer().teleport(Bukkit.getWorld("Spawn").getSpawnLocation());
                p = Bukkit.getPlayer(name_player[0]);
                p.sendMessage("§a§lVous avez gagné §c§lXX §a§lElo ! Ainsi que la partie !");
                p.teleport(Bukkit.getWorld("Spawn").getSpawnLocation());
                p.getInventory().clear();
                p.getInventory().setContents((plugin.inv.get(p)).getContents());
                plugin.inv.remove(e.getPlayer());
                plugin.inv.remove(p);
            } else if (name_player[1].equalsIgnoreCase(e.getPlayer().getName())) {
                e.getPlayer().teleport(Bukkit.getWorld("Spawn").getSpawnLocation());
                e.getPlayer().getInventory().clear();
                e.getPlayer().getInventory().setContents(plugin.inv.get(e.getPlayer()).getContents());
                plugin.onArene.remove(s);
                p = Bukkit.getPlayer(name_player[0]);
                p.sendMessage("§a§lVous avez gagné XX Elo ! Ainsi que la partie !");
                p.teleport(Bukkit.getWorld("Spawn").getSpawnLocation());
                p.getInventory().clear();
                p.getInventory().setContents(plugin.inv.get(e.getPlayer()).getContents());
                plugin.inv.remove(e.getPlayer());
                plugin.inv.remove(p);
            }
        }

    }

    @EventHandler
    public void on(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        //sql update info members = new String[]{Bukkit.getPlayerExact(p.getName()).getUniqueId().toString(), p.getName(), "0", "0", "", "", "true", "true", "true"};
        p.sendMessage("§c§lAttention§7§:§a§l Vous êtes sur une version §4§lBêta §a§ldu serveur §b§lGlorCraft §7§l(§c§lFaction§7§) !");
        p.sendMessage("§4§lA la fin de cette bêta, tout les joueurs recommenceront de 0, pour (re) commencer à jouer sans les bugs !");
        p.sendMessage("§a§lSi vous trouvez un bug, contactez un membre du Staff, ou faite un POST sur notre forum : http://forum.glorycraft.fr/");
        if (e.getPlayer().getWorld().getName().contains("Hub")) {
            e.getPlayer().teleport(Bukkit.getWorld("Hub").getSpawnLocation());
            e.getPlayer().getInventory().clear();
            setting = new ItemStack(Material.EYE_OF_ENDER, 1);
            ItemMeta i = setting.getItemMeta();
            i.setDisplayName(ChatColor.RED + "Vos paramètres. " + ChatColor.GRAY + " (Version Bêta)");
            setting.setItemMeta(i);
            game = new ItemStack(Material.COMPASS, 1);
            ItemMeta i1 = game.getItemMeta();
            i1.setDisplayName(ChatColor.YELLOW + "Choix du Jeux.");
            game.setItemMeta(i1);
            gadgets = new ItemStack(Material.BLAZE_POWDER, 1);
            ItemMeta i3 = gadgets.getItemMeta();
            i3.setDisplayName(ChatColor.AQUA + "Gadgets " + ChatColor.GRAY + " (Version bêta)");
            gadgets.setItemMeta(i3);
            shop = new ItemStack(Material.DIAMOND, 1);
            ItemMeta i4 = shop.getItemMeta();
            i4.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
            i4.setDisplayName(ChatColor.DARK_AQUA + "Boutique " + ChatColor.GRAY + " (Version bêta)");
            shop.setItemMeta(i4);
            e.getPlayer().getInventory().setItem(0, setting);
            e.getPlayer().getInventory().setItem(1, game);
            e.getPlayer().getInventory().setItem(7, gadgets);
            e.getPlayer().getInventory().setItem(8, shop);
        }

    }
}
