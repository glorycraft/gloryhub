package fr.glorycraft.hub.events.players;

import fr.glorycraft.hub.GloryHub;
import fr.glorycraft.hub.utils.InventoryEvent;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;

public class PlayerInventoryClickListener {
    Hashtable<Player, BukkitTask> r = new Hashtable<>();
    Hashtable<String, String> Invo = new Hashtable<>();
    private GloryHub plugin;

    public PlayerInventoryClickListener(GloryHub main) {
        plugin = main;
    }

    @EventHandler
    public void onclick(InventoryClickEvent e) {
        final Player p = (Player) e.getWhoClicked();
        Inventory inven = e.getClickedInventory();
        if (e.getCurrentItem() != null) {
            File ItemCustoms;
            ItemStack ia;
            ItemMeta iam;
            if (inven.getTitle().contains("PvPBox")) {
                e.setCancelled(true);
                final File repertoire = new File("./PvPBox");
                final String[] listefichiers = repertoire.list();

                for (int i = 0; i < listefichiers.length; ++i) {
                    if (listefichiers[i].endsWith(".yml")) {
                        ItemCustoms = new File(repertoire, listefichiers[i]);
                        final FileConfiguration fc = YamlConfiguration.loadConfiguration(ItemCustoms);
                        String[] a = fc.getString("inv").split(":");
                        ia = new ItemStack(Material.getMaterial(Integer.parseInt(a[0])), Integer.parseInt(a[1]));
                        iam = ia.getItemMeta();
                        iam.setDisplayName(a[2].equals("no") ? iam.getDisplayName() : ChatColor.translateAlternateColorCodes('&', a[2]));
                        iam.addEnchant(Enchantment.getById(Integer.parseInt(a[3])), Integer.parseInt(a[4]), true);
                        int nb = 0;

                        if (plugin.pvpbox.isEmpty()) {
                            nb = 0;
                        } else {


                            for (String name : plugin.pvpbox) {

                                String[] name1 = name.split(":");
                                if (name1[1].contains(ItemCustoms.getName())) {
                                    ++nb;
                                }
                            }
                        }

                        int playerInMAP = 0;
                        if (Bukkit.getWorld(ItemCustoms.getName()) != null) {
                            playerInMAP = Bukkit.getWorld(ItemCustoms.getName()).getPlayers().size();
                        }

                        iam.setLore(Arrays.asList("§e§lNombre de joueur :", "§a§l" + playerInMAP, "§c§l En attente :", "§c§l" + nb));
                        ia.setItemMeta(iam);
                        if (e.getCurrentItem().isSimilar(ia)) {
                            if (p.hasPermission(fc.getString("perm"))) {
                                plugin.pvpbox.add(e.getWhoClicked().getName() + ":" + listefichiers[i]);
                                String name = listefichiers[i];
                                final Player player = (Player) e.getWhoClicked();
                                this.r.put(player, Bukkit.getScheduler().runTaskTimer(plugin, () -> {
                                    int nummin = 0;
                                    int nummax = 9;
                                    p.sendMessage("§e§lRecherche entre §c§l" + nummin + " §e§l et §c§l" + nummax);

                                    //Iterator var3 = plugin.pvpbox.iterator();


                                    Player adv;
                                    int i1;
                                    String[] a1;
                                    ItemStack ia1;
                                    ItemMeta iam1;
                                    YamlConfiguration fcx;


                                    for(String s: plugin.pvpbox) {


                                        //   String px = (String) var3.next();
                                        String[] name1 = s.split(":");
                                        if (player.getName().equals(name1[0]) && (!name1[1].contains(name))) {
                                            adv = Bukkit.getPlayer(name1[0]);
                                            player.sendMessage("§e§lNous avons trouvée une partie, vous jouer contre : §c§l" + name1[0]);
                                            adv.sendMessage("§e§lNous avons trouvée une partie, vous jouer contre : §c§l" + player.getName());
                                            int i22 = 0;


                                            for (String ar : plugin.onArene) {

                                                String[] name2 = ar.split(":");
                                                if (name2[0].contains(name)) {
                                                    ++i22;
                                                    if (i22 > 9) {
                                                        nummin += 9;
                                                        nummax += 9;
                                                    }
                                                }
                                            }

                                            if (fc.get("arene") != null) {
                                                if (fc.getStringList("arene").get(i22) != null) {
                                                    r.get(adv).cancel();
                                                    r.get(player).cancel();
                                                    plugin.pvpbox.remove(player.getName() + ":" + name);
                                                    plugin.pvpbox.remove(adv.getName() + ":" + name);
                                                    plugin.onArene.add(name + ":" + adv.getName() + "-" + player.getName());
                                                    Random rl = new Random();
                                                    player.teleport(plugin.str2loc(fc.getStringList("arene").get(i22)).add((double) rl.nextInt(20), 0.0D, (double) rl.nextInt(20)));
                                                    adv.teleport(plugin.str2loc(fc.getStringList("arene").get(i22)).add((double) rl.nextInt(20), 0.0D, (double) rl.nextInt(20)));
                                                    plugin.inv.put(adv, adv.getInventory());
                                                    plugin.inv.put(player, player.getInventory());
                                                    adv.getInventory().clear();
                                                    player.getInventory().clear();
                                                    File fi = new File(repertoire, listefichiers[i22]);
                                                    fcx = YamlConfiguration.loadConfiguration(fi);
                                                    ItemStack[] armor = null;
                                                    String[] enchf;
                                                    String[] var17;
                                                    int var18;
                                                    int var19;
                                                    String s;
                                                    String[] enchfx;
                                                    if (fcx.getStringList("armor") != null && !fcx.getStringList("armor").isEmpty()) {
                                                        armor = new ItemStack[fcx.getStringList("armor").size()];

                                                        for (i1 = fcx.getStringList("armor").size() - 1; i1 >= 0; --i1) {
                                                            a1 = (fcx.getStringList("armor").get(i1)).split(":");
                                                            ia1 = new ItemStack(Material.getMaterial(Integer.parseInt(a1[0])), Integer.parseInt(a1[1]));
                                                            iam1 = ia1.getItemMeta();
                                                            iam1.setDisplayName(a1[2].equals("no") ? iam1.getDisplayName() : ChatColor.translateAlternateColorCodes('&', a1[2]));
                                                            if (a1.length > 3) {
                                                                if (a1[3].contains(",")) {
                                                                    enchf = a1[3].split(",");
                                                                    var17 = enchf;
                                                                    var18 = enchf.length;

                                                                    for (var19 = 0; var19 < var18; ++var19) {
                                                                        s = var17[var19];
                                                                        enchfx = s.split(";");
                                                                        iam1.addEnchant(Enchantment.getById(Integer.parseInt(enchfx[0])), Integer.parseInt(enchfx[1]), true);
                                                                    }
                                                                } else {
                                                                    enchf = a1[3].split(";");
                                                                    iam1.addEnchant(Enchantment.getById(Integer.parseInt(enchf[0])), Integer.parseInt(enchf[1]), true);
                                                                }

                                                                ia1.setItemMeta(iam1);
                                                            }

                                                            armor[i1] = ia1;
                                                        }

                                                        player.getInventory().setArmorContents(armor);
                                                        adv.getInventory().setArmorContents(armor);
                                                    }

                                                    if (fcx.getStringList("items") != null) {
                                                        for (i1 = fcx.getStringList("items").size() - 1; i1 >= 0; --i1) {
                                                            a1 = fcx.getStringList("items").get(i1).split(":");
                                                            ia1 = new ItemStack(Material.getMaterial(Integer.parseInt(a1[0])), Integer.parseInt(a1[1]));
                                                            iam1 = ia1.getItemMeta();
                                                            iam1.setDisplayName(a1[2].equals("no") ? iam1.getDisplayName() : ChatColor.translateAlternateColorCodes('&', a1[2].replace("_", " ")));
                                                            if (a1.length > 3) {
                                                                if (a1[3].contains(",")) {
                                                                    enchf = a1[3].split(",");
                                                                    var17 = enchf;
                                                                    var18 = enchf.length;

                                                                    for (var19 = 0; var19 < var18; ++var19) {
                                                                        s = var17[var19];
                                                                        enchfx = s.split(";");
                                                                        iam1.addEnchant(Enchantment.getById(Integer.parseInt(enchfx[0])), Integer.parseInt(enchfx[1]), true);
                                                                    }
                                                                } else {
                                                                    enchf = a1[3].split(";");
                                                                    iam1.addEnchant(Enchantment.getById(Integer.parseInt(enchf[0])), Integer.parseInt(enchf[1]), true);
                                                                }
                                                            }

                                                            ia1.setItemMeta(iam1);
                                                            player.getInventory().addItem(ia1);
                                                            adv.getInventory().addItem(ia1);
                                                        }
                                                    }

                                                }

                                                adv.sendMessage("§e§lAucune arène disponible, retenté plus tard.");
                                                player.sendMessage("§e§lAucune arène disponible, retenté plus tard.");
                                                r.get(adv).cancel();
                                                r.get(player).cancel();
                                                plugin.pvpbox.remove(player.getName() + ":" + name);
                                                plugin.pvpbox.remove(adv.getName() + ":" + name);
                                            } else {
                                                adv.sendMessage("§c§lUne erreur est arrivé.");
                                                player.sendMessage("§c§lUne erreur est arrivé.");
                                                r.get(adv).cancel();
                                                r.get(player).cancel();
                                                plugin.pvpbox.remove(player.getName() + ":" + name);
                                                plugin.pvpbox.remove(adv.getName() + ":" + name);
                                            }


                                            for (i1 = fcx.getStringList("itemspo").size() - 1; i1 >= 0; --i1) {
                                                a1 = fcx.getStringList("itemspo").get(i1).split(":");
                                                ia1 = new ItemStack(Material.getMaterial(Integer.parseInt(a1[0])), Integer.parseInt(a1[2]), (short) ((int) Long.parseLong(a1[1])));
                                                iam1 = ia1.getItemMeta();
                                                iam1.setDisplayName(a1[3].equals("no") ? iam1.getDisplayName() : ChatColor.translateAlternateColorCodes('&', a1[3].replace("_", " ")));
                                                ia1.setItemMeta(iam1);
                                                player.getInventory().addItem(ia1);
                                                adv.getInventory().addItem(ia1);
                                            }
                                        }
                                    }


                                }, 0L, 20L));
                                p.sendMessage(ChatColor.translateAlternateColorCodes('&', fc.getString("msg")));
                            } else {
                                p.sendMessage(ChatColor.translateAlternateColorCodes('&', fc.getString("noperm")));
                            }
                        }
                    }
                }
            }



            if (p.getWorld().getName().contains("Hub")) {
                InventoryEvent inve = new InventoryEvent(e.getCurrentItem(), (Player) e.getWhoClicked());
                ItemStack i1;
                ItemMeta i1m;
                ItemStack i2;
                ItemStack test;
                if (inven.getTitle().contains("Gadgets")) {
                    ItemStack i;
                    ItemMeta im;
                    if (e.getCurrentItem().isSimilar(this.item2)) {
                        p.sendMessage(ChatColor.RED + "Attention: L'item que vous avez sélectionné est une version bêta, il pourrait y avoir des bugs.");
                        e.setCancelled(true);
                        Inventory i1 = Bukkit.createInventory(null, 9, ChatColor.AQUA + "Vos Chapeaux.");
                        ItemStack i2 = new ItemStack(Material.GLASS);
                        ItemMeta i2m = i2.getItemMeta();
                        i2m.setDisplayName("Chapeau en verre");
                        i2.setItemMeta(i2m);
                        i = new ItemStack(Material.PUMPKIN);
                        im = i.getItemMeta();
                        im.setDisplayName(ChatColor.RED + "Citrouille");
                        i.setItemMeta(im);
                        ia = new ItemStack(Material.GRASS);
                        iam = ia.getItemMeta();
                        iam.setDisplayName(ChatColor.GREEN + "Chapeau en herbe");
                        ia.setItemMeta(iam);
                        test = new ItemStack(Material.DIRT);
                        ItemMeta i5m = test.getItemMeta();
                        i5m.setDisplayName("Chapeau en Terre");
                        test.setItemMeta(i5m);
                        i1.addItem(i2);
                        i1.addItem(i);
                        i1.addItem(ia);
                        i1.addItem(test);
                        p.openInventory(i1);
                    }

                    i1 = new ItemStack(Material.BONE, 1);
                    i1m = i1.getItemMeta();
                    i1m.setDisplayName(ChatColor.AQUA + "Pets " + ChatColor.GRAY + " (Version bêta)");
                    i1.setItemMeta(i1m);
                    if (e.getCurrentItem().isSimilar(i1)) {
                        p.sendMessage(ChatColor.RED + "Attention: L'item que vous avez sélectionné est une version bêta, il pourrait y avoir des bugs.");
                        e.setCancelled(true);
                        Inventory i2 = Bukkit.createInventory(null, 9, ChatColor.AQUA + "Vos Pets.");
                        i = new ItemStack(Material.DIAMOND_BARDING, 1);
                        im = i.getItemMeta();
                        im.setDisplayName(ChatColor.AQUA + "Vos montures.");
                        i.setItemMeta(im);
                        i2.addItem(i);
                        i = new ItemStack(Material.MONSTER_EGG, 1);
                        im = i.getItemMeta();
                        im.setDisplayName("Métamorphoses (Bêta)");
                        i.setItemMeta(im);
                        i2.addItem(i);
                        i = new ItemStack(Material.DRAGON_EGG, 1);
                        im = i.getItemMeta();
                        im.setDisplayName("Famillier");
                        i.setItemMeta(im);
                        i2.addItem(i);
                        p.openInventory(i2);
                    }

                    i2 = new ItemStack(Material.BLAZE_POWDER, 1);
                    i1m = i2.getItemMeta();
                    i1m.setDisplayName(ChatColor.AQUA + "Effets " + ChatColor.GRAY + " (Version bêta)");
                    i2.setItemMeta(i1m);
                    if (e.getCurrentItem().isSimilar(i2)) {
                        p.sendMessage(ChatColor.RED + "Attention: L'item que vous avez sélectionné est une version bêta, il pourrait y avoir des bugs.");
                        e.setCancelled(true);
                        Inventory i2 = Bukkit.createInventory(null, 54, ChatColor.AQUA + "Vos Effets.");
                        Effect[] var32 = Effect.values();
                        int var33 = var32.length;

                        for (int var39 = 0; var39 < var33; ++var39) {
                            Effect e1 = var32[var39];
                            switch (e1) {
                                case CLOUD:
                                    i1 = new ItemStack(Material.BONE, 1);
                                    i1m = i1.getItemMeta();
                                    i1m.setDisplayName(ChatColor.AQUA + "Nuages ");
                                    i1.setItemMeta(i1m);
                                    i2.addItem(i1);
                                    break;
                                case FLYING_GLYPH:
                                    i1 = new ItemStack(Material.ENCHANTMENT_TABLE, 1);
                                    i1m = i1.getItemMeta();
                                    i1m.setDisplayName(ChatColor.AQUA + "Echantements ");
                                    i1.setItemMeta(i1m);
                                    i2.addItem(i1);
                                    break;
                                case MOBSPAWNER_FLAMES:
                                    i1 = new ItemStack(Material.MOB_SPAWNER, 1);
                                    i1m = i1.getItemMeta();
                                    i1m.setDisplayName(ChatColor.AQUA + "MobSpawner Flammes ");
                                    i1.setItemMeta(i1m);
                                    i2.addItem(i1);
                                    break;
                                default:
                                    i1 = new ItemStack(Material.MOB_SPAWNER, 1);
                                    i1m = i1.getItemMeta();
                                    i1m.setDisplayName(ChatColor.RED + e1.getName());
                                    i1.setItemMeta(i1m);
                                    i2.addItem(i1);
                            }
                        }

                        p.openInventory(i2);
                    }
                }

                if (inven.getTitle().contains("Vos Effets")) {
                    p.sendMessage(ChatColor.RED + "Attention: L'item que vous avez sélectionné est une version bêta, il pourrait y avoir des bugs.");
                    e.setCancelled(true);
                    i1 = new ItemStack(Material.FIREWORK, 1);
                    i1m = i1.getItemMeta();
                    i1m.setDisplayName(ChatColor.AQUA + "Nuages ");
                    i1.setItemMeta(i1m);
                    Effect[] var25 = Effect.values();
                    int var30 = var25.length;

                    for (int var35 = 0; var35 < var30; ++var35) {
                        Effect e1 = var25[var35];
                        switch (e1) {
                            case CLOUD:
                                i1 = new ItemStack(Material.BONE, 1);
                                i1m = i1.getItemMeta();
                                i1m.setDisplayName(ChatColor.AQUA + "Nuages ");
                                i1.setItemMeta(i1m);
                                if (e.getCurrentItem().isSimilar(i1)) {
                                    if (plugin.eff.get(p) != null) {
                                        plugin.eff.remove(p);
                                    }

                                    plugin.eff.put(p, e1);
                                    p.sendMessage(ChatColor.BOLD + " Vous avez chois l'effet nuages .");
                                    p.closeInventory();
                                }
                                break;
                            case FLYING_GLYPH:
                                i1 = new ItemStack(Material.ENCHANTMENT_TABLE, 1);
                                i1m = i1.getItemMeta();
                                i1m.setDisplayName(ChatColor.AQUA + "Enchantements ");
                                i1.setItemMeta(i1m);
                                if (e.getCurrentItem().isSimilar(i1)) {
                                    if (plugin.eff.get(p) != null) {
                                        plugin.eff.remove(p);
                                    }

                                    plugin.eff.put(p, e1);
                                    p.sendMessage(ChatColor.BOLD + "" + ChatColor.GRAY + " Vous avez chois l'effet Enchantements .");
                                    p.closeInventory();
                                }
                                break;
                            case MOBSPAWNER_FLAMES:
                                i1 = new ItemStack(Material.MOB_SPAWNER, 1);
                                i1m = i1.getItemMeta();
                                i1m.setDisplayName(ChatColor.AQUA + "MobSpawner Flammes ");
                                i1.setItemMeta(i1m);
                                if (e.getCurrentItem().isSimilar(i1)) {
                                    if (plugin.eff.get(p) != null) {
                                        plugin.eff.remove(p);
                                    }

                                    plugin.eff.put(p, e1);
                                    plugin.eff.put(p, e1);
                                    p.sendMessage(ChatColor.BOLD + "" + ChatColor.RED + " Vous avez chois l'effet MobSpawner Flammes .");
                                    p.closeInventory();
                                }
                                break;
                            default:
                                i1 = new ItemStack(Material.MOB_SPAWNER, 1);
                                i1m = i1.getItemMeta();
                                i1m.setDisplayName(ChatColor.RED + e1.getName());
                                i1.setItemMeta(i1m);
                                if (e.getCurrentItem().isSimilar(i1)) {
                                    if (plugin.eff.get(p) != null) {
                                        plugin.eff.remove(p);
                                    }

                                    plugin.eff.put(p, e1);
                                    p.sendMessage(ChatColor.BOLD + "" + ChatColor.GRAY + " Vous avez chois l'effet " + e1.getName() + " .");
                                    plugin.eff.put(p, e1);
                                    p.closeInventory();
                                }
                        }
                    }
                }

                if (inven.getTitle().contains("Param")) {
                    p.sendMessage(ChatColor.RED + "Attention: L'item que vous avez sélectionné est une version bêta, il pourrait y avoir des bugs.");
                    e.setCancelled(true);
                    i1 = new ItemStack(Material.PAPER);
                    i1m = i1.getItemMeta();
                    if (plugin.isMsg(p)) {
                        i1m.setDisplayName(ChatColor.GRAY + "Messages Privés(" + ChatColor.GREEN + "Activer" + ChatColor.GRAY + ")");
                    } else {
                        i1m.setDisplayName(ChatColor.GRAY + "Messages Privés (" + ChatColor.RED + " Désactiver " + ChatColor.GRAY + ")");
                    }

                    i1.setItemMeta(i1m);
                    Wool w = new Wool();
                    if (plugin.isMsg(p)) {
                        w.setColor(DyeColor.GREEN);
                        i1 = w.toItemStack();
                        i1m = i1.getItemMeta();
                        i1m.setDisplayName(ChatColor.GRAY + "Désactiver les messages privés.");
                    } else {
                        w.setColor(DyeColor.RED);
                        i1 = w.toItemStack();
                        i1m = i1.getItemMeta();
                        i1m.setDisplayName(ChatColor.GRAY + "Activer les messages privés.");
                    }

                    i1.setItemMeta(i1m);
                    i1.setAmount(1);
                    if (e.getCurrentItem().isSimilar(i1)) {
                        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Activer")) {
                            plugin.msgA(p, true);
                        }

                        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Désactiver")) {
                            plugin.msgA(p, false);
                        }
                    }

                    i1 = new ItemStack(Material.MAP);
                    i1m = i1.getItemMeta();
                    if (plugin.isChat(p)) {
                        i1m.setDisplayName(ChatColor.GRAY + "Chat Hub(" + ChatColor.GREEN + "Activer" + ChatColor.GRAY + ")");
                    } else {
                        i1m.setDisplayName(ChatColor.GRAY + "Chat Hub(" + ChatColor.RED + " Désactiver " + ChatColor.GRAY + ")");
                    }

                    i1.setItemMeta(i1m);
                    if (plugin.isChat(p)) {
                        w.setColor(DyeColor.GREEN);
                        i1 = w.toItemStack();
                        i1m = i1.getItemMeta();
                        i1m.setDisplayName(ChatColor.GRAY + "Désactiver le chat du(des) hub(s).");
                    } else {
                        w.setColor(DyeColor.RED);
                        i1 = w.toItemStack();
                        i1m = i1.getItemMeta();
                        i1m.setDisplayName(ChatColor.GRAY + "Activer le chat du(des) hub(s).");
                    }

                    i1.setItemMeta(i1m);
                    i1.setAmount(1);
                    if (e.getCurrentItem().isSimilar(i1)) {
                        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Activer")) {
                            plugin.hubA(p, true);
                        }

                        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Désactiver")) {
                            plugin.hubA(p, false);
                        }
                    }

                    i1 = new ItemStack(Material.SKULL_ITEM);
                    i1m = i1.getItemMeta();
                    if (plugin.isFriendA(p)) {
                        i1m.setDisplayName(ChatColor.GRAY + "Demandes d'amis(" + ChatColor.GREEN + "Activer" + ChatColor.GRAY + ")");
                    } else {
                        i1m.setDisplayName(ChatColor.GRAY + "Demandes d'amis(" + ChatColor.RED + " Désactiver " + ChatColor.GRAY + ")");
                    }

                    i1.setItemMeta(i1m);
                    if (plugin.isFriendA(p)) {
                        w.setColor(DyeColor.GREEN);
                        i1 = w.toItemStack();
                        i1m = i1.getItemMeta();
                        i1m.setDisplayName(ChatColor.GRAY + "Désactiver les demandes d'amis");
                    } else {
                        w.setColor(DyeColor.RED);
                        i1 = w.toItemStack();
                        i1m = i1.getItemMeta();
                        i1m.setDisplayName(ChatColor.GRAY + "Activer les demandes d'amis");
                    }

                    i1.setItemMeta(i1m);
                    i1.setAmount(1);
                    if (e.getCurrentItem().isSimilar(i1)) {
                        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Activer")) {
                            plugin.friendA(p, true);
                        }

                        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Désactiver")) {
                            plugin.friendA(p, false);
                        }
                    }

                    e.setCancelled(true);
                    p.closeInventory();
                }

                if (e.getCurrentItem() == this.item) {
                    p.sendMessage(ChatColor.RED + "Erreur : Action demander indisponible");
                }

                if (p.getItemInHand().isSimilar(this.item1)) {
                }

                if (p.getItemInHand().isSimilar(this.item3)) {
                }

                if (e.getCurrentItem().isSimilar(this.item4)) {
                    InventoryShop invy = new InventoryShop(p, GloryHub.ItemsShop, plugin);
                    invy.Open();
                }

                FileConfiguration f = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "games.yml"));
                String title = ChatColor.translateAlternateColorCodes('&', f.getString("title"));
                if (title.contains("=>")) {
                    title = title.replace("=>", "➡");
                }

                if (title.contains("<=")) {
                    title = title.replace("<=", "⬅");
                }

                if (title.contains("^|")) {
                    title = title.replace("^|", "⬆");
                }

                if (title.contains("|^")) {
                    title = title.replace("|^", "⬇");
                }

                if (title.contains("<3")) {
                    title = title.replace("<3", "❤");
                }

                if (title.contains("[x]")) {
                    title = title.replace("[x]", "█");
                }

                if (title.contains("[++]")) {
                    title = title.replace("[++]", "✦");
                }

                if (title.contains("[+]")) {
                    title = title.replace("[+]", "◆");
                }

                if (title.contains("[p]")) {
                    title = title.replace("[p]", "•");
                }

                if (title.contains("[/]")) {
                    title = title.replace("[/]", "▌");
                }

                if (title.contains("[*]")) {
                    title = title.replace("[*]", "★");
                }

                if (title.contains("[**]")) {
                    title = title.replace("[**]", "✹");
                }

                if (title.contains("[v]")) {
                    title = title.replace("[v]", "✔");
                }

                if (title.contains("[c]")) {
                    title = title.replace("[c]", "✠");
                }

                String title2;
                ItemStack i4;
                if (inven.getTitle().contains(title)) {
                    e.setCancelled(true);
                    ConfigurationSection cs = f.getConfigurationSection("compass");
                    Iterator var31 = cs.getKeys(false).iterator();

                    while (var31.hasNext()) {
                        String key = (String) var31.next();
                        title2 = ChatColor.translateAlternateColorCodes('&', cs.getString(key + ".name"));
                        i4 = CreateItem(p.getName(), cs.getString(key + ".id"), title2, cs.getStringList(key + ".lore"), cs.getInt(key + ".amount"), false);
                        if (e.getCurrentItem().isSimilar(i4) && cs.get(key + ".x") != null && cs.get(key + ".y") != null && cs.get(key + ".z") != null) {
                            p.teleport(new Location(Bukkit.getWorld("Hub"), (double) cs.getInt(key + ".x"), (double) cs.getInt(key + ".y"), (double) cs.getInt(key + ".z")));
                        }
                    }
                }

                if (inven.getTitle().contains("Chapeau")) {
                    e.setCancelled(true);
                    i2 = new ItemStack(Material.GLASS);
                    ItemMeta i2m = i2.getItemMeta();
                    i2m.setDisplayName("Chapeau en verre");
                    i2.setItemMeta(i2m);
                    ItemStack i3 = new ItemStack(Material.PUMPKIN);
                    ItemMeta i3m = i3.getItemMeta();
                    i3m.setDisplayName(ChatColor.RED + "Citrouille");
                    i3.setItemMeta(i3m);
                    i4 = new ItemStack(Material.GRASS);
                    ItemMeta i4m = i4.getItemMeta();
                    i4m.setDisplayName(ChatColor.GREEN + "Chapeau en herbe");
                    i4.setItemMeta(i4m);
                    ItemStack i5 = new ItemStack(Material.DIRT);
                    ItemMeta i5m = i5.getItemMeta();
                    i5m.setDisplayName("Chapeau en Terre");
                    i5.setItemMeta(i5m);
                    if (e.getCurrentItem().isSimilar(i2)) {
                        p.getEquipment().setHelmet(i2);
                        p.closeInventory();
                        p.sendMessage(ChatColor.GREEN + "Chapeau mis !");
                    }

                    if (e.getCurrentItem().isSimilar(i3)) {
                        p.getEquipment().setHelmet(i3);
                        p.closeInventory();
                        p.sendMessage(ChatColor.GREEN + "Chapeau mis !");
                    }

                    if (e.getCurrentItem().isSimilar(i4)) {
                        p.getEquipment().setHelmet(i4);
                        p.closeInventory();
                        p.sendMessage(ChatColor.GREEN + "Chapeau mis !");
                    }

                    if (e.getCurrentItem().isSimilar(i5)) {
                        p.getEquipment().setHelmet(i5);
                        p.closeInventory();
                        p.sendMessage(ChatColor.GREEN + "Chapeau mis !");
                    }
                }

                if (inven.getTitle().contains("Pets")) {
                    inve.openPets();
                    e.setCancelled(true);
                }

                if (inven.getTitle().contains("Famillier")) {
                    e.setCancelled(true);
                    inve.deguisInv(dType.FAM);
                }

                if (inven.getTitle().contains("Métamorphose")) {
                    e.setCancelled(true);
                    inve.deguisInv(dType.DEGUIS);
                }

                if (inven.getTitle().contains("Montures")) {
                    e.setCancelled(true);
                    inve.deguisInv(dType.RIDE);
                }

                ItemCustoms = null;
                f = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "shop.yml"));
                String title1 = ChatColor.translateAlternateColorCodes('&', f.getString("title"));
                if (inven.getTitle().contains(title1)) {
                    e.setCancelled(true);
                    ConfigurationSection cs = f.getConfigurationSection("shop");
                    Iterator var43 = cs.getKeys(false).iterator();

                    while (var43.hasNext()) {
                        String key = (String) var43.next();
                        test = CreateItem(p.getName(), cs.getString(key + ".id"), cs.getString(key + ".name"), cs.getStringList(key + ".lore"), cs.getInt(key + ".amount"), false);
                        if (e.getCurrentItem().isSimilar(test) && cs.getString(key + ".action") != null) {
                            if (cs.getString(key + ".action").contains("SOON")) {
                                p.sendMessage("Cet articles n'est pas encore disponible.");
                                p.closeInventory();
                            } else {
                                File f1 = new File(plugin.getDataFolder() + "/shops_inv/", cs.getString(key + ".action") + ".yml");
                                if (f1.exists()) {
                                    f = YamlConfiguration.loadConfiguration(f1);
                                    CustomInventory invy = new CustomInventory(f, p, plugin, "items");
                                    p.openInventory(invy.getInv());
                                    this.Invo.put(p.getName(), cs.getString(key + ".action"));
                                }
                            }
                        }
                    }
                }

                if (this.Invo.get(p.getName()) != null) {
                    FileConfiguration f1 = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder() + "/shops_inv/", this.Invo.get(p.getName()) + ".yml"));
                    title2 = ChatColor.translateAlternateColorCodes('&', f1.getString("title"));
                    if (inven.getTitle().contains(title2)) {
                        p.sendMessage(ChatColor.RED + "Attention: L'item que vous avez sélectionné est une version bêta, il pourrait y avoir des bugs.");
                        e.setCancelled(true);
                        ConfigurationSection cs = f1.getConfigurationSection("c");
                        Iterator var59 = cs.getKeys(false).iterator();

                        while (var59.hasNext()) {
                            String key = (String) var59.next();
                            ItemStack test = CreateItem(p.getName(), cs.getString(key + ".id"), cs.getString(key + ".name"), cs.getStringList(key + ".lore"), cs.getInt(key + ".amount"), false);
                            if (e.getCurrentItem().isSimilar(test)) {
                            }
                        }
                    }
                }
            }
        }

    }
}