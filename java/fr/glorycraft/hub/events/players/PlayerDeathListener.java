package fr.glorycraft.hub.events.players;

import fr.glorycraft.hub.GloryHub;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.Iterator;

public class PlayerDeathListener implements Listener {

    private GloryHub plugin;

    public PlayerDeathListener(GloryHub main) {
        plugin = main;
    }


    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        Iterator var2 = plugin.onArene.iterator();

        for (String s : plugin.onArene) {
            Player death;
            Player p;

            String[] info = s.split(":");
            String[] name_player = info[1].split("-");
            if (name_player[0].equalsIgnoreCase(e.getEntity().getPlayer().getName())) {
                death = e.getEntity().getPlayer();
                Bukkit.getPlayer(death.getUniqueId()).teleport(Bukkit.getWorld("Faction").getSpawnLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
                death.sendMessage("§c§lDommage, vous avez perdue la partie !");
                p = Bukkit.getPlayer(name_player[1]);
                p.sendMessage("§a§lVous avez gagné §c§lXX §a§lElo ! Ainsi que la partie !");
                p.teleport(Bukkit.getWorld("Spawn").getSpawnLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
                p.getInventory().clear();
                p.getInventory().setContents((plugin.inv.get(p)).getContents());
                death.getInventory().clear();
                death.getInventory().setContents((plugin.inv.get(death)).getContents());
                plugin.inv.remove(death);
                plugin.inv.remove(p);
                death.setHealth(20.0D);
                death.setFireTicks(0);
                death.setFoodLevel(20);
                plugin.onArene.remove(s);
                return;
            } else if (name_player[1].equalsIgnoreCase(e.getEntity().getPlayer().getName())) {
                death = e.getEntity().getPlayer();
                Bukkit.getPlayer(death.getUniqueId()).teleport(Bukkit.getWorld("Faction").getSpawnLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
                death.sendMessage("§c§lDommage, vous avez perdue la partie !");
                p = Bukkit.getPlayer(name_player[0]);
                p.sendMessage("§a§lVous avez gagné §c§lXX §a§lElo ! Ainsi que la partie !");
                p.teleport(Bukkit.getWorld("Spawn").getSpawnLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
                p.getInventory().clear();
                p.getInventory().setContents((plugin.inv.get(p)).getContents());
                death.getInventory().clear();
                death.getInventory().setContents((plugin.inv.get(death)).getContents());
                plugin.inv.remove(death);
                plugin.inv.remove(p);
                death.setHealth(20.0D);
                death.setFireTicks(0);
                death.setFoodLevel(20);
                death.setExp(death.getExp());
                plugin.onArene.remove(s);
            }
        }


    }
}
