package fr.glorycraft.hub.events.players;

import fr.glorycraft.hub.GloryHub;
import fr.glorycraft.hub.inventorys.InventoryCompass;
import fr.glorycraft.hub.inventorys.InventoryShop;
import fr.glorycraft.hub.items.ItemCompass;
import fr.glorycraft.hub.items.ItemShop;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.Arrays;

public class PlayerInteractListener implements Listener {

    public static String n = "thekikoo";
    private GloryHub plugin;
    private ItemStack hatItem;

    public PlayerInteractListener(GloryHub main) {
        plugin = main;
    }


    @EventHandler
    public void on(PlayerInteractEvent e) {
        // if (!plugin.isLogin(e.getPlayer().getName().toLowerCase())) {
        //      e.setCancelled(true);
        //  }

        hatItem = new ItemStack(Material.DIAMOND_HELMET, 1);
        ItemMeta i2 = hatItem.getItemMeta();
        i2.setDisplayName(ChatColor.AQUA + "Vos Chapeaux.");
        this.hatItem.setItemMeta(i2);


        if (e.getPlayer().getWorld().getName().contains("Hub")) {
            if (e.getPlayer().getItemInHand().isSimilar(PlayerJoinAndLeaveListener.setting)) {
                Inventory i = Bukkit.createInventory(null, 27, ChatColor.RED + "Vos Paramètres.");
                ItemStack i1 = new ItemStack(Material.PAPER);
                ItemMeta i1m = i1.getItemMeta();
               /* if (plugin.isMsg(e.getPlayer())) {
                    i1m.setDisplayName(ChatColor.GRAY + "Messages Privés(" + ChatColor.GREEN + "Activer" + ChatColor.GRAY + ")");
                } else {
                    i1m.setDisplayName(ChatColor.GRAY + "Messages Privés (" + ChatColor.RED + " Désactiver " + ChatColor.GRAY + ")");
                }*/

                i1.setItemMeta(i1m);
                i.setItem(0, i1);
                //     Wool w = new Wool();
               /* if (plugin.isMsg(e.getPlayer())) {
                    w.setColor(DyeColor.GREEN);
                    i1 = w.toItemStack();
                    i1m = i1.getItemMeta();
                    i1m.setDisplayName(ChatColor.GRAY + "Désactiver les messages privés.");
                } else {
                    w.setColor(DyeColor.RED);
                    i1 = w.toItemStack();
                    i1m = i1.getItemMeta();
                    i1m.setDisplayName(ChatColor.GRAY + "Activer les messages privés.");
                }*/

                i1.setItemMeta(i1m);
                i1.setAmount(1);
                i.setItem(18, i1);
                i1 = new ItemStack(Material.MAP);
                i1m = i1.getItemMeta();
              /*  if (plugin.isChat(e.getPlayer())) {
                    i1m.setDisplayName(ChatColor.GRAY + "Chat Hub(" + ChatColor.GREEN + "Activer" + ChatColor.GRAY + ")");
                } else {
                    i1m.setDisplayName(ChatColor.GRAY + "Chat Hub(" + ChatColor.RED + " Désactiver " + ChatColor.GRAY + ")");
                }*/

                i1.setItemMeta(i1m);
                i.setItem(2, i1);
                /*if (plugin.isChat(e.getPlayer())) {
                    w.setColor(DyeColor.GREEN);
                    i1 = w.toItemStack();
                    i1m = i1.getItemMeta();
                    i1m.setDisplayName(ChatColor.GRAY + "Désactiver le chat du(des) hub(s).");
                } else {
                    w.setColor(DyeColor.RED);
                    i1 = w.toItemStack();
                    i1m = i1.getItemMeta();
                    i1m.setDisplayName(ChatColor.GRAY + "Activer le chat du(des) hub(s).");
                }
*/
                i1.setItemMeta(i1m);
                i1.setAmount(1);
                i.setItem(20, i1);
                i1 = new ItemStack(Material.SKULL_ITEM);
                i1m = i1.getItemMeta();
               /* if (plugin.isChat(e.getPlayer())) {
                    i1m.setDisplayName(ChatColor.GRAY + "Demandes d'amis(" + ChatColor.GREEN + "Activer" + ChatColor.GRAY + ")");
                } else {
                    i1m.setDisplayName(ChatColor.GRAY + "Demandes d'amis(" + ChatColor.RED + " Désactiver " + ChatColor.GRAY + ")");
                }
*/
                i1.setItemMeta(i1m);
                i.setItem(4, i1);
               /* if (plugin.isFriendA(e.getPlayer())) {
                    w.setColor(DyeColor.GREEN);
                    i1 = w.toItemStack();
                    i1m = i1.getItemMeta();
                    i1m.setDisplayName(ChatColor.GRAY + "Désactiver les demandes d'amis");
                } else {
                    w.setColor(DyeColor.RED);
                    i1 = w.toItemStack();
                    i1m = i1.getItemMeta();
                    i1m.setDisplayName(ChatColor.GRAY + "Activer les demandes d'amis");
                }
*/
                i1.setItemMeta(i1m);
                i1.setAmount(1);
                i.setItem(22, i1);
                e.getPlayer().openInventory(i);
                e.setCancelled(true);
            }

            if (e.getPlayer().getItemInHand().isSimilar(PlayerJoinAndLeaveListener.game)) {
                n = e.getPlayer().getName();
                ItemCompass I = new ItemCompass(plugin, e.getPlayer());
                GloryHub.ItemsCompass = I.BuildItems();
                InventoryCompass invy = new InventoryCompass(e.getPlayer(), GloryHub.ItemsCompass, plugin);
                invy.Open();
                e.setCancelled(true);
            }

            if (e.getPlayer().getItemInHand().isSimilar(PlayerJoinAndLeaveListener.gadgets)) {
                ItemStack i3 = new ItemStack(Material.BONE, 1);
                ItemMeta i3m = i3.getItemMeta();
                i3m.setDisplayName(ChatColor.AQUA + "Pets " + ChatColor.GRAY + " (Version bêta)");
                i3.setItemMeta(i3m);
                e.getPlayer().sendMessage(ChatColor.RED + "Attention: L'item que vous avez sélectionné est une version bêta, il pourrait y avoir des bugs.");
                Inventory i1 = Bukkit.createInventory(null, 9, ChatColor.AQUA + "Vos Gadgets.");

                i1.addItem(i3);


                i3 = new ItemStack(Material.BLAZE_POWDER, 1);
                i3m = i3.getItemMeta();
                i3m.setDisplayName("§b§lEffets  §7§l(Version bêta)");
                i3.setItemMeta(i3m);
                i1.addItem(i3);
                e.getPlayer().openInventory(i1);
                e.setCancelled(true);
            }

            if (e.getPlayer().getItemInHand().isSimilar(PlayerJoinAndLeaveListener.shop)) {
                ItemShop I = new ItemShop(plugin, e.getPlayer());
                GloryHub.ItemsShop = I.BuildItems();
                InventoryShop invy = new InventoryShop(e.getPlayer(), GloryHub.ItemsShop, plugin);
                invy.Open();
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(
            priority = EventPriority.HIGH
    )
    public void onClickSign(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getState() instanceof Sign) {
            Sign sign = (Sign)e.getClickedBlock().getState();
            String s = sign.getLine(2);
            if (s.contains("PvPBox")) {
                Inventory i = Bukkit.createInventory(null, 9, "§b§LChoix du Kit/de la MAP PvPBox");
                File repertoire = new File("./PvPBox");
                ItemStack[] item = new ItemStack[9];
                String[] listefichiers = repertoire.list();

                for(int i1 = 0; i1 < listefichiers.length; ++i1) {
                    if (listefichiers[i1].endsWith(".yml")) {
                        File fi = new File(repertoire, listefichiers[i1]);
                        FileConfiguration fc = YamlConfiguration.loadConfiguration(fi);
                        String[] a = fc.getString("inv").split(":");
                        ItemStack ia = new ItemStack(Material.getMaterial(Integer.parseInt(a[0])), Integer.parseInt(a[1]));
                        ItemMeta iam = ia.getItemMeta();
                        iam.setDisplayName(a[2].equals("no") ? iam.getDisplayName() : ChatColor.translateAlternateColorCodes('&', a[2]));
                        iam.addEnchant(Enchantment.getById(Integer.parseInt(a[3])), Integer.parseInt(a[4]), true);
                        int nb = 0;
                        if (plugin.pvpbox.isEmpty()) {
                            nb = 0;
                        } else {


                            for(String p: plugin.pvpbox) {

                                String[] name = p.split(":");
                                if (name[1].contains(fi.getName())) {
                                    ++nb;
                                }
                            }
                        }

                        int JoueurInMAP = 0;
                        if (Bukkit.getWorld(fi.getName()) != null) {
                            JoueurInMAP = Bukkit.getWorld(fi.getName()).getPlayers().size();
                        }

                        iam.setLore(Arrays.asList("§e§lNombre de joueur :", "§a§l" + JoueurInMAP, "§c§l En attente :", "§c§l" + nb));
                        ia.setItemMeta(iam);
                        i.addItem(new ItemStack[]{ia});
                    }
                }

                player.openInventory(i);
            }
        }

    }
}
