package fr.glorycraft.hub.events.players;

import fr.glorycraft.hub.GloryHub;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class OtherPlayerListener {
    private GloryHub plugin;

    public OtherPlayerListener(GloryHub main) {
        plugin = main;
    }


    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        //     if (!plugin.isLogin(event.getPlayer().getName().toLowerCase())) {
        //       ;
        // }

        if (event.getPlayer().getWorld().getName().contains("Hub")) {
            event.setCancelled(true);
        }

    }

    @EventHandler
    public void onMoveEvent(PlayerMoveEvent e) {
        String worldname = e.getPlayer().getWorld().getName();
        if ((e.getPlayer().isFlying() && worldname.contains("PvP") || worldname.contains("Faction") || worldname.equals("Gloryfary")) && !e.getPlayer().hasPermission("fly.bypass")) {
            e.getPlayer().setFlying(false);
        }

     /*   if (!plugin.isLogin(e.getPlayer().getName())) {
            Location loc = e.getTo();
            e.getPlayer().teleport(loc, PlayerTeleportEvent.TeleportCause.PLUGIN);
        }*/


        if (e.getPlayer().getWorld().getName().contains("Hub") && e.getPlayer().getLocation().getBlockY() <= -1) {
            e.getPlayer().teleport(Bukkit.getWorld("Hub").getSpawnLocation());
        }

    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent e) {
        if (e.getMessage().equals("/spawn") && e.getPlayer().getWorld().getName().contains("FreeBuild")) {
            e.setCancelled(true);
            e.getPlayer().teleport(e.getPlayer().getWorld().getSpawnLocation());
        }


        for (String s : plugin.onArene) {
            String[] name;


            String[] info = s.split(":");
            name = info[1].split("-");
            if (!name[0].equalsIgnoreCase(e.getPlayer().getName()) && !name[1].equalsIgnoreCase(e.getPlayer().getName())) {


                e.setCancelled(true);
                e.getPlayer().sendMessage("§c§lAucune commandes n'est permisse dans le PvPBox.");

            }
        }

    }



   /* public void onChatEvent(AsyncPlayerChatEvent event) {
        event.setFormat(ChatColor.translateAlternateColorCodes('&', ChatColor.WHITE + event.getFormat()));
    }*/
}
