package fr.glorycraft.hub.items;

import fr.glorycraft.hub.GloryHub;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.Hashtable;
import java.util.List;

public class ItemShop {
    public static Player player;
    public static GloryHub plugin;
    private Hashtable<Integer, ItemStack> items = new Hashtable<>();

    public ItemShop(GloryHub gloryHub, Player p) {
        player = p;

        plugin = gloryHub;
    }

    public Hashtable<Integer, ItemStack> BuildItems() {
        FileConfiguration f = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "shop.yml"));
        ConfigurationSection cs = f.getConfigurationSection("shop");

        for (String key : cs.getKeys(false)) {

            int place = 0;

            try {
                place = Integer.valueOf(key) - 1;
            } catch (Exception var7) {
                System.out.println("Failed to create item with ID of " + cs.getString(key + ".id") + " @ place " + key);
            }

            ItemStack item = CreateItem(cs.getString(key + ".id"), cs.getString(key + ".name"), cs.getStringList(key + ".lore"), cs.getInt(key + ".amount"));
            if (item != null && place < 53) {
                this.items.put(place, item);
            }
        }

        return this.items;
    }

    @Nullable
    private ItemStack CreateItem(String id, String name, List<String> lore, int amount) {

        String[] data = id.split(":");


        int mit;
        try {
            mit = Integer.valueOf(data[0]);
        } catch (Exception var13) {
            System.out.println(ChatColor.RED + "[Erreur]" + ChatColor.WHITE + "Failed to create item with ID of " + id);
            return null;
        }

        ItemStack is = new ItemStack(Material.getMaterial(mit));
        ItemMeta im = is.getItemMeta();
        switch (name) {
            case "=>":
                name = name.replace("=>", "➡");
                break;
            case "<=":
                name = name.replace("<=", "⬅");
                break;
            case "^|":
                name = name.replace("^|", "⬆");
                break;
            case "|^":
                name = name.replace("|^", "⬇");
                break;
            case "<3":
                name = name.replace("<3", "❤");
                break;
            case "[x]":
                name = name.replace("[x]", "█");
                break;
            case "[++]":
                name = name.replace("[++]", "✦");
                break;
            case "[+]":
                name = name.replace("[+]", "◆");
                break;
            case "[p]":
                name = name.replace("[p]", "•");
                break;
            case "[/]":
                name = name.replace("[/]", "▌");
                break;
            case "[*]":
                name = name.replace("[*]", "★");
                break;
            case "[**]":
                name = name.replace("[**]", "✹");
                break;
            case "[v]":
                name = name.replace("[v]", "✔");
                break;
            case "[c]":
                name = name.replace("[c]", "✠");
                break;
            case "%ge":
                name = name.replace("%ge", "§e" + -1);
                break;
            case "%gc":
                name = name.replace("%gc", "§e" + -1);
                break;
        }

        im.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        if (data.length == 2) {
            is.setDurability(Short.valueOf(data[1]));
        }

        if (amount > 0) {
            is.setAmount(amount);
        }

        if (lore != null) {
            for (String l : lore) {
                lore.remove(l);
                l = ChatColor.translateAlternateColorCodes('&', l);

                if (l.contains("%ge")) {
                    l = l.replace("%ge", ChatColor.YELLOW + "" + -1);
                }

                if (l.contains("%gc")) {
                    l = l.replace("%gc", ChatColor.YELLOW + "" + -1);
                }
                lore.add(l);
            }

            im.setLore(lore);
        }

        is.setItemMeta(im);
        return is;
    }

}
