package fr.glorycraft.hub.items;

import fr.glorycraft.hub.GloryHub;
import fr.glorycraft.hub.events.players.PlayerInteractListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.io.File;
import java.util.Hashtable;
import java.util.List;

public class ItemCompass {
    public static Player player = null;
    private Hashtable<Integer, ItemStack> items = new Hashtable<>();
    private GloryHub plugin;

    public ItemCompass(GloryHub gloryHub, Player p) {
        player = p;

        plugin = gloryHub;
    }

    public static ItemStack CreateItem(String id, String name, List<String> lore, int amount, boolean glow) {
        int gc = 0;
        int ge = 0;
        String[] data = id.split(":");


        int mit;


        mit = Integer.valueOf(data[0]);


        ItemStack is = new ItemStack(Material.getMaterial(mit));

        if (id.contains("397")) {
            SkullMeta meta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
            if (data.length == 2) {
                is.setDurability(Short.valueOf(data[1]));
            }

            if (amount > 0) {
                is.setAmount(amount);
            }

            if (lore != null) {


                for (String l : lore) {
                    lore.remove(l);
                    l = ChatColor.translateAlternateColorCodes('&', l);
                    if (l.contains("%ge")) {
                        l = l.replace("%ge", ChatColor.YELLOW + "" + ge);
                    }

                    if (l.contains("%gc")) {
                        l = l.replace("%gc", ChatColor.YELLOW + "" + gc);
                    }
                    lore.add(l);
                }

                meta.setLore(lore);
            }
            meta.setOwner(PlayerInteractListener.n);
            is.setItemMeta(meta);
        } else {
            ItemMeta im = is.getItemMeta();
            im.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
            if (data.length == 2) {
                is.setDurability(Short.parseShort(data[1]));
            }

            if (amount > 0) {
                is.setAmount(amount);
            }

            if (lore != null) {

                im.setLore(lore);
            }

            is.setItemMeta(im);
        }

        return is;
    }

    public Hashtable<Integer, ItemStack> BuildItems() {

        FileConfiguration f = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "games.yml"));
        ConfigurationSection cs = f.getConfigurationSection("compass");

        for (String key : cs.getKeys(false)) {
            int place = 0;

            try {
                place = Integer.valueOf(key) - 1;
            } catch (Exception var10) {
                System.out.println("Failed to create item with ID of " + cs.getString(key + ".id") + " @ place " + key);

            }

            String name = ChatColor.translateAlternateColorCodes('&', cs.getString(key + ".name"));
            switch (name) {
                case "=>":
                    name = name.replace("=>", "➡");
                    break;
                case "<=":
                    name = name.replace("<=", "⬅");
                    break;
                case "^|":
                    name = name.replace("^|", "⬆");
                    break;
                case "|^":
                    name = name.replace("|^", "⬇");
                    break;
                case "<3":
                    name = name.replace("<3", "❤");
                    break;
                case "[x]":
                    name = name.replace("[x]", "█");
                    break;
                case "[++]":
                    name = name.replace("[++]", "✦");
                    break;
                case "[+]":
                    name = name.replace("[+]", "◆");
                    break;
                case "[p]":
                    name = name.replace("[p]", "•");
                    break;
                case "[/]":
                    name = name.replace("[/]", "▌");
                    break;
                case "[*]":
                    name = name.replace("[*]", "★");
                    break;
                case "[**]":
                    name = name.replace("[**]", "✹");
                    break;
                case "[v]":
                    name = name.replace("[v]", "✔");
                    break;
                case "[c]":
                    name = name.replace("[c]", "✠");
                    break;
                case "%ge":
                    name = name.replace("%ge", "§e" + -1);
                    break;
                case "%gc":
                    name = name.replace("%gc", "§e" + -1);
                    break;
            }

            ItemStack item = CreateItem(cs.getString(key + ".id"), name, cs.getStringList(key + ".lore"), cs.getInt(key + ".amount"), false);
            if (item != null && place < 53) {
                this.items.put(place, item);
            }
        }

        return this.items;
    }
}
